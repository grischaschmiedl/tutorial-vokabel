This is a small vocabulary trainer based on Ionic 2. 
It demonstrates a simple MVC-app thats avoids untyped variables and untyped javascript-"classes"

Features:
* using a provider (injectable) as "data-root"
* TypeScript-Class(es) = Business-Objects organized in a separate folder
* trivial interface with list, inputfields, buttons