import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Data} from '../../providers/data';
import { VokabelPage } from '../vokabel-page/vokabel-page';
import {Vokabel} from '../../models/vokabel';

/*
  Generated class for the ListPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-list-page',
  templateUrl: 'list-page.html'
})
export class ListPage {

  public vokabelListe: Vokabel[];
  public lektionName: String;

  constructor(public navCtrl: NavController, private data: Data) {
    this.vokabelListe = data.currentLektion.liste;
    this.lektionName = data.currentLektion.name;
    console.log(this.vokabelListe);
  }

  ionViewDidLoad() {
    console.log('Hello ListPage Page');
  }

  showVokabel(vokabel: Vokabel) {
    console.log(vokabel);
    this.navCtrl.push(VokabelPage, {mode: "existing", item: vokabel});
  }

  newVokabel() {
    this.navCtrl.push(VokabelPage, {mode: "new", item: null});
  }

  delete(vokabel) {
    let index = this.vokabelListe.indexOf(vokabel);
    if(index > -1){ this.vokabelListe.splice(index, 1); 
      this.data.save();
    }
  }

}
