import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Vokabel} from '../../models/vokabel';
import {Data} from '../../providers/data';

/*
  Generated class for the VokabelPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-vokabel-page',
  templateUrl: 'vokabel-page.html'
})
export class VokabelPage {

  public vokabel: Vokabel;
  public orig: string = "";
  public trans: string = "";
  public zusatz;
  public mode: string;

  constructor(public navCtrl: NavController, private navParams: NavParams, public data: Data) {
    //debugger;
    this.mode = navParams.get('mode');
    if (this.mode=="existing") {
      this.vokabel = navParams.get('item');
      this.orig = this.vokabel.original;
      this.zusatz = this.vokabel.zusatz;
      this.trans = this.vokabel.translation;
    } else {

    }
  }

  save() {
    if (this.mode=="existing") {
      this.vokabel.original = this.orig;
      this.vokabel.zusatz = this.zusatz;
      this.vokabel.translation = this.trans;
    } else {
      let vok = new Vokabel(this.orig, this.zusatz, this.trans);
      this.data.currentLektion.liste.push(vok);
    }
    this.data.save();
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('Hello VokabelPage Page');
  }

}
