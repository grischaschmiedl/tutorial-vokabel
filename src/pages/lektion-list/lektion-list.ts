import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Lektion } from '../../models/lektion';
import {Data} from '../../providers/data';
import { LektionPage } from '../lektion/lektion';

/*
  Generated class for the SektionList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-lektion-list',
  templateUrl: 'lektion-list.html'
})
export class LektionListPage {

  public get lektionListe(): Lektion[] {return this.data.lektionen};

  constructor(public navCtrl: NavController, private data: Data) {
  }

  ionViewDidLoad() {
    console.log('Hello SektionListPage Page');
  }

  showLektion(lektion: Lektion) {
    console.log(lektion);
    this.navCtrl.push(LektionPage, {mode: "existing", item: lektion});
  }

  ActivateLektion(lektion: Lektion) {
    //debugger;
    let index = this.data.lektionen.indexOf(lektion);
    if(index > -1){ 
      this.data.currentLektionNumber = index; 
      this.navCtrl.pop();
    }
  }

  newLektion() {
    this.navCtrl.push(LektionPage, {mode: "new", item: null});
  }

  delete(lektion) {
    let index = this.data.lektionen.indexOf(lektion);
    if(index > -1){ this.data.lektionen.splice(index, 1); 
      this.data.save();
    }
  }

}
