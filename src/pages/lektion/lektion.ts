import { Lektion } from '../../models/lektion';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Data} from '../../providers/data';

/*
  Generated class for the Lektion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-lektion',
  templateUrl: 'lektion.html'
})
export class LektionPage {

  public lektion: Lektion;
  public name: string = "";
  public json: string = "";
  public message: string = "";
  public mode: string;

  constructor(public navCtrl: NavController, private navParams: NavParams, public data: Data) {
    this.mode = navParams.get('mode');
    if (this.mode=="existing") {
      this.lektion = navParams.get('item');
      this.name = this.lektion.name;
    }
  }

  ionViewDidLoad() {
    console.log('Hello LektionPage Page');
  }

  save() {
    if (this.mode=="existing") {
      this.lektion.name = this.name;
    } else {
      let lek = new Lektion(this.name);
      this.data.lektionen.push(lek);
    }
    this.data.save();
    this.navCtrl.pop();
  }

  import() {
    let newLektion = Lektion.deserializeFromString(this.json, this.name);
    if (newLektion) {
      this.data.lektionen.push(newLektion);
      this.data.save();
      this.navCtrl.pop();
    } else {
      this.message = "Import fehlegeschlagen!"
    }
  }

}
