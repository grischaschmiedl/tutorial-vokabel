import { LektionListPage } from '../lektion-list/lektion-list';
import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { ListPage } from '../list-page/list-page';
import { TestPage } from '../test-page/test-page';

import {Data} from '../../providers/data';
//import {Vokabel} from '../../models/vokabel';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public get vokabelInLektion(): number {
    if (this.data.currentLektion && this.data.currentLektion.liste)
      return this.data.currentLektion.liste.length;
    else return 0;
  };
  public get anzahlLektionen(): number {
    if (this.data.lektionen)
      return this.data.lektionen.length;
    else return 0;
  };
  public get currentLektionName(): string {
    if (this.data.currentLektion) 
      return this.data.currentLektion.name;
    else return "";
  };

  constructor(public navCtrl: NavController, public data: Data) {
    //this.anzahlLektionen = this.data.lektionen.length;
    //this.vokabelInLektion = this.data.currentLektion.liste.length;
    //this.currentLektionName = this.data.currentLektion.name;
  }

  public showList() {
    this.navCtrl.push(ListPage);
  }

  public showLektionList() {
    this.navCtrl.push(LektionListPage);
  }

  public showTest() {
    this.navCtrl.push(TestPage);
  }

  public resetTest() {
    this.data.lektionen[0].liste.forEach(element => {
      element.known = false;
    });
    this.data.save();
  }
}
