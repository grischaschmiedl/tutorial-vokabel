import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {Data} from '../../providers/data';
import {Vokabel} from '../../models/vokabel';

@Component({
  selector: 'page-test-page',
  templateUrl: 'test-page.html'
})
export class TestPage {

  public testVokabel: Vokabel[] = [];
  public currentVokabel: Vokabel = null;
  public get gesamtAnzahl(): number {return this.data.currentLektion.liste.length}
  public get lektionName(): string {return this.data.currentLektion.name}
  public mode:number = 0;

  constructor(public navCtrl: NavController, private data: Data) {
    this.data.currentLektion.liste.forEach((element: Vokabel) => {
      if (!element.known) this.testVokabel.push(element);
    });
    console.log("Offene Vokabel: " + this.testVokabel.length);
    this.getNext();
  }

  getNext() {
    if (this.testVokabel.length>0) {
      let index = Math.floor(Math.random()*this.testVokabel.length)
      this.currentVokabel = this.testVokabel[index];
      console.log("Vokabel: " + this.currentVokabel);
      this.mode=0;
    } else this.mode=2; //fertig
  }

  setKnown() {
    if (this.currentVokabel) {
      this.currentVokabel.known = true;
      this.data.save();
      let index = this.testVokabel.indexOf(this.currentVokabel);
      if(index > -1) this.testVokabel.splice(index, 1);
    }
    this.getNext();
  }

  ionViewDidLoad() {
    console.log('Hello TestPage Page');
  }

}
