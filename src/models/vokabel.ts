export class Vokabel {


  constructor(public original: string,
              public zusatz: string,
              public translation: string,
              public known: boolean = false) {

  }

  public static deserialize(rawVokabel: any): Vokabel {
    console.log("Vokabel.deserialize called with:");
    console.log(rawVokabel);
    if(typeof(rawVokabel) != "undefined") {
      return new Vokabel(rawVokabel.original, rawVokabel.zusatz, rawVokabel.translation, rawVokabel.known);
    }
    else {
      return null;
    }
  }

  public static deserializeArray(rawVokabelListe: any): Vokabel[] {
    console.log("Vokabel.deserializeArray called with:");
    console.log(rawVokabelListe);
    if (rawVokabelListe && Array.isArray(rawVokabelListe)) {
      let vokabelArray = [];
      rawVokabelListe.forEach(element => {
        vokabelArray.push(Vokabel.deserialize(element));
      });
      return vokabelArray;
    } else
      return null;
  }


}

