import { Vokabel } from './vokabel';
export class Lektion {

  public name: string;
  public liste: Vokabel[];

  constructor(name: string) {
    this.name = name;
    this.liste = [];
  }

  public static deserialize(rawLektion: any): Lektion {
    console.log("Lektion.deserialize called with:");
    console.log(rawLektion);
    if(typeof(rawLektion) != "undefined") {
      let lektion = new Lektion(rawLektion.name);
      lektion.liste = Vokabel.deserializeArray(rawLektion.liste);
      return lektion;
    }
    else {
      return null;
    }
  }

  public static deserializeArray(rawLektionenListe: any): Lektion[] {
    console.log("Lektion.deserializeArray called with:");
    console.log(rawLektionenListe);
    if (rawLektionenListe && Array.isArray(rawLektionenListe)) {
      let lektionenArray = [];
      rawLektionenListe.forEach(element => {
        lektionenArray.push(Lektion.deserialize(element));
      });
      console.log(lektionenArray);
      return lektionenArray;
    } else
      return null;
  }

  public static deserializeFromString(strVokListe: string, name: string): Lektion {
    let rawVok = JSON.parse(strVokListe);
    if (rawVok) {
      let newLektion = new Lektion(name);
      newLektion.liste = Vokabel.deserializeArray(rawVok);
      if (newLektion.liste)
        return newLektion
      else
        return null;
    } else {
      return null;
    }
  }

}

