import { Lektion } from '../models/lektion';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';
//import {Vokabel} from '../models/vokabel';

@Injectable()
export class Data {

public lektionen: Lektion[] = [];
public currentLektionNumber = 0;
public get currentLektion(): Lektion {
  //debugger;
  if (this.currentLektionNumber>=0)
    return this.lektionen[this.currentLektionNumber]
  else
    return null;
}

  constructor(public http: Http, private storage: Storage) {
    console.log('Hello Data Provider');
    //this.initValues();
    this.load();
  }

  save() {
    let saveString = JSON.stringify(this.lektionen);
    console.log("save: "+saveString);
    this.storage.set('lektionen', saveString);
  }

  load() {
    this.lektionen = [];
    this.storage.get('lektionen').then(rawLektionen => {
      //debugger;
        this.lektionen = Lektion.deserializeArray(JSON.parse(rawLektionen));
        console.log("Lektionen geladen:")
        console.log(this.lektionen);
        if (this.lektionen==null){
          this.lektionen = [];
          this.lektionen.push(new Lektion("1"));
        }
        console.log("Lektionen geladen:");
        console.log(this.lektionen);
    });
  }



}
