import { LektionPage } from '../pages/lektion/lektion';
import { LektionListPage } from '../pages/lektion-list/lektion-list';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

//Neu eintragen:
import { Storage } from '@ionic/storage';
import { VokabelPage } from '../pages/vokabel-page/vokabel-page';
import { ListPage } from '../pages/list-page/list-page';
import { TestPage } from '../pages/test-page/test-page';
import { Data } from '../providers/data';
import {FlashCardComponent} from '../components/flash-card/flash-card'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    TestPage,
    VokabelPage,
    LektionListPage,
    LektionPage,
    FlashCardComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    TestPage,
    VokabelPage,
    LektionListPage,
    LektionPage
  ],
  providers: [Storage, Data, { provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule {}
